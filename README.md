# ![Municipality Project Tracker](docs/images/mptlogo-light-vector.svg)

This web application allows municipalities in Nepal to track project schedules and budget allocations. It was developed by [Kirtipur Municipality](http://kirtipurmun.gov.np/) and is available for use and extension for any organisation that might benefit from it.

This repository holds the Docker configuration for development and deployment, using git submodules to handle version changes for each of the webapp's subsystems.

## Features
This software will allow you to do the following:

* Create projects, which are attributed to committees
* Add one or many activities to a project, allocating spend and establishing schedules for delivery timing
* Manage committee membership, adding personnel along with contact information
* Set reminders for schedule deadlines

## Subsystems
The Municipality Project Tracker is not a monolithic webapp, and is comprised of subsystems which can be developed independently. The purpose of this is to allow front-end feature development to occur independently of back-end feature development—including the development of non-web based front-ends (such as mobile, device and desktop applications). The two subsystems used by the webapp are as follows:

The following subsystems are used by the webapp:

* **AdminUI** (`adminui/`) — a front-end interface for the MPT
    * [Create React App](https://facebook.github.io/create-react-app/) — Built on the Create React App framework.
    * [React 16](https://reactjs.org/) — This app makes use of functional components, hooks, etc.
    * [Material UI](https://material-ui.com/) — A React library for UI components based on Google's Material UI.
* **API** (`api/`) — a RESTful web service for handling data
    * [ASP.NET Core 2.2](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.2) — Microsoft's official multi-platform implementation of .NET
    * [C# 8](https://docs.microsoft.com/en-us/dotnet/csharp/) — Microsoft's flagship object-oriented programming language.
    * [EntityFrameworkCore (EFCore)](https://docs.microsoft.com/en-us/ef/core/) — Connects to a MySQL database using EFCore as an ORM solution.
    * [MySQL EFCore Connector](https://dev.mysql.com/doc/connector-net/en/connector-net-entityframework-core.html) — The database connector to interface with the MySQL server.

Additionally, the following servers are used to host and deploy the web application:

* [Docker](https://docker.com/) — An app container tool to help isolate services on a common infrastructure. Using Docker allows us to standardise the build environments across platforms.
* [MySQL 8](https://www.mysql.com/products/community/) — The RDBMS used by the API to store data.
* [Nginx](https://www.nginx.com/) — The web server application used to host AdminUI.

## Development
This repository is helpful for setting up the latest version of each subsystem. Once each subsystem is pulled from version control, you may consult the READMEs of each subsystem to learn how to set up your development environment to develop that particular subsystem.

This repos is also used for deployment (see [Deployment](#Deployment) below), so that releases can be deployed to production with specific versions of each subsystem. The section below ([Getting started](#getting-started)) outlines how to use this repo to download the latest version of each subsystem to your local machine and build each subsystem using Docker compose.

### Getting started
This repository is the best place to begin when developing the Municiplaity Project Tracking software. As explained in the [Subsystems](#subsystems) section, the API webservice is available in the `api/` git submodule, and the front-end is available in the `adminui/` git submodule.
Each of these subsystems have their own git repo, along with detailed instructions for development contained within their README files. Following this instructions in this repo will allow you to clone and build the latest production versions of each subsystem, launching a MySQL server for the API subsystem to use as a datastore.

Building and hosting each of these subsystems on your local machine will not require any SDKs to be installed on your host machine as they will be included in the Docker containers. Once you have a working instance of the webapp you may continue development on these subsystems (which will require the appropriate SDK to be installed on your machine).

Follow the instructions below to download the latest version of each subsystem, to get a working front-end, along with a MySQL Server and API webserver. Each subsystem will be built using a build container (which will be destroyed once built), and then served from within a container. Below is a diagram of the complete web application detailing which containers host which subsystem:
![MPT running instance architecture diagram, showing which Docker containers serve which subsystem of the MPT](docs/images/diagrams/MPT%20Architecture.png)

#### Prerequisites
You will need to have [Docker](https://docker.com/) installed on your local machine to host this web application locally. Once installed, you can follow the instructions below to build and host the API, AdminUI, and a MySQL server instance.

#### 1. Clone this repository and initialise submodules
If you have write access to Kirtipur Municipality's Team on BitBucket, you should use the `ssh` protocol to clone this repo, otherwise the `https` protocol will suffice.

```
git clone https://bitbucket.org/kirtipurmun/mpt.git
```

This will download the Docker configurations, as well as a development environment file into an `mpt` directory. Each subsystem will not be present as they are independent git repositories. They are designated git submodules however, so to retreive the latest production versions of each, run the following commands:

```
cd mpt
git submodule init
git submodule update
```

This will initialise the submodules into empty `api/` and `'adminui` directories, and then proceed to download tbe `master` version of each to their respective directories.

#### 2. Run Docker compose
With the `MPT_ENVIRONMENT` environment variable set, you'll be able to run the Docker swarm to build and host the webapp and its subsystems. You may run it in detached mode using the `-d` flag, however we'll be running it in interactive mode to help trace any errors for a first run:

```
docker-compose up
```

This step may take some time as Docker will need to download the ASP.NET Core 2.2 SDK, ASP.NET Core 2.2 Runtime, Node.js 12 Runtime, Nginx and MySQL images. Once the images are ready, the build process will download dependencies from .NET Core and Node.js's package managers so that they compile production-ready versions of each subsystem. Each of these steps should be reported to the console to help track progress.

#### 3. Visit the AdminUI URL in your web browser
Once each of the docker containers are online, you may visit the AdminUI URL in your web browser. For a default development build, this will be:

[http://localhost:3000](http://localhost:3000)

From there may experiment with the webapp to see how it behaves.

## Deployments
This repository is also used to coordinate deployments for the production version of Municiplaity Project Tracker. Deploying to a production build is similar to the process of building and hosting for deployment, but with a few key differences.

1. Production `.env` files are not version controlled
2. .NET Core and Create React App are configured for production mode

Deployment information to follow...

# License
MIT License

Copyright (c) 2019 Kirtipur Municipality

You may use and extend this software for whatever purpose you desire, as long as you leave the copyrights and acknowledgements in place. See LICENCE for details.